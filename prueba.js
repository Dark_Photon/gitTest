const printer = require('./lib/node-thermal-printer');

var express = require('express');
var bodyParser = require('body-parser');
var _ = require('lodash');
var moment = require('moment');

const PRINTERTYPES = {
    KITCHEN: 'Kitchen',
    CLIENT: 'Client'
};


var app = express();

app.use(bodyParser.text())

app.post('/', async (req, res) => {

    //console.log(req.body);
    const data = JSON.parse(req.body);
    console.log(data)
    const impresoras = data.system.printerMachines;

    for (let impresora of impresoras) {

        if(impresora.deviceType.toLowerCase() === data.mode || data.mode === 'all') {
            console.log(impresora.deviceConnection);
            var ip = (impresora.deviceConnection === 'NETWORK' ? 'tcp://' : '\\\\localhost\\') + impresora.deviceIP;
            console.log("Imprimiendo ticket")
            console.log(ip);
            await printPull(ip, data, impresora.deviceType);
        }

    }

    res.send("OK");

});

app.listen(8001, () => {
    console.log("Servidor iniciado");
});


function orderByCat(items,cats) {

    var newArray = [];
  
    console.log("orderByCat")
    //Order by category, "Starter", "Main" and "Desserts" and pushed in newArray[]
    cats.forEach((cat) => {
      newArray.push(items.filter((item) => {
        return item.categoryType === cat;
      }));
    });

    //It find a products without category y pushed to newArray[]
    newArray.push(items.filter((item) => {
      return cats.indexOf(item.categoryType) < 0;
      }));
      
    return newArray;
    //var ordered = items.sort((a,b) => cats.indexOf(a.categoryType) - cats.indexOf(b.categoryType))
}

  function orderByCatClone(items,cats) {
    

    var newArray = [];
  
    //Order by category, "Starter", "Main" and "Desserts" and pushed in newArray[]
    
    cats.forEach((cat, pos) => {

        newArray.push({id: 'print', typeC: cat})

        newArray.push(items.filter((item) => {
        return item.categoryType === cat;
        }));

    });
  
    newArray.push({id: 'print', typeC: 'Others'})

    //It find a products without category y pushed to newArray[]
    newArray.push(items.filter((item) => {
      return cats.indexOf(item.categoryType) < 0;
    }));
     
    return newArray;
    //var ordered = items.sort((a,b) => cats.indexOf(a.categoryType) - cats.indexOf(b.categoryType))
  }

//////////////////////
//////////////////////
//////////////////////
//////////////////////
//////////////////////

function orderByCatCloneKitchen(items,cats, kitchen = false) {

    var newArray = [];
  
    //Order by category, "Starter", "Main" and "Desserts" and pushed in newArray[]
    cats.forEach((cat) => {

        newArray.push({id: 'print', typeC: cat})

        newArray.push(items.filter((item) => {
            return item.categoryType === cat;
        }));

    });
  
    newArray.push({id: 'print', typeC: 'Others'})

    //It find a products without category y pushed to newArray[]
    
    newArray.push(items.filter((item) => {

        if(cats.indexOf(item.categoryType) < 0){
            item.categoryType = 'Others';
        } 

        return cats.indexOf(item.categoryType) < 0;
    }));

    return newArray;
    
  }



/////////////////////////////
function printPull(ip, datos, printerType) {

    var {
        system,
        categories,
        items,
        currentTab,
        taxRates,
        change,
        paymentMethod,
        total,
        receiptCardData,
        mode,
        offerData
    } = datos;

    // console.log('items')
    // console.log(items);

    //Modifica la propiedad "isPrinted" a ---FALSE--- para que se imprima todos los productos
    mode === 'all' || mode === 'client'? items.forEach(element => {
        element.isPrinted = false;
    }): console.log("No es mode === all, todo como siempre")


    mode === 'client' ? console.log("modo CLIENTE"): console.log();
    mode === 'kitchen' ? console.log("modo KITCHEN"): console.log();
    // mode === 'all' ? items.forEach(element => {
    //     element.isPrinted = false;
    // }): console.log("No es mode === all, todo como siempre")
    items = items.reverse();

     //

    let cats = ['Starter', 'Main', 'Dessert'];
    let vectorCategory = orderByCat(items,cats);
    let itemsOrdered= [];

    vectorCategory.forEach((vector)=>{
        itemsOrdered = _.concat(itemsOrdered, vector)
    })


    // let vectorCopy = orderByCatClone(items,cats);
    let vectorCopy = orderByCatCloneKitchen(items,cats,true);
    let itemsOrderedCategory= [];
    
    // Convert vectorCategory = [starters][Main][Desserts][Others]
    vectorCopy.forEach((vector)=>{
        itemsOrderedCategory = _.concat(itemsOrderedCategory, vector)
    })

    ///prueba que puede ser borradadaaaa

    let todo = orderByCatCloneKitchen(items,cats, true);
    let itemsPrueba = [];
    todo.forEach((vector)=>{
        itemsPrueba = _.concat(itemsPrueba, vector)
    })

    //console.log('itemsPrueba')
    //console.log(itemsPrueba)
    let otro = itemsPrueba.filter((item)=>{
        // return ((typeof (item.isPrinted) === 'undefined'|| item.productDiffQuantity > 0) && item.id !== 'print');
        return ( (typeof (item.isPrinted) === 'undefined' || item.isPrinted === false || item.productDiffQuantity > 0));
    })

    console.log(`otro: ${otro.length}` )
    console.log(otro)


    let starterArray = items.filter((item)=>item.categoryType === 'Starter')
    let mainArray = items.filter( item => item.categoryType === 'Main')
    let dessertArray = items.filter( item => item.categoryType === 'Dessert')
    let othersArray = items.filter((item) => {
        if(cats.indexOf(item.categoryType) < 0){
            item.categoryType = 'Others';
        } 
        return cats.indexOf(item.categoryType) < 0;
    });
   
 
    
    ////////////
   

    const companyDetails = _.get(system, 'companyDetails', {});
    const receiptOptions = _.get(system, 'receiptOptions', {});
    const showCompanyDetails = _.includes(receiptOptions, true);
    const vatItems = _.map(taxRates, taxRate => {
      const taxRateItems = _.filter(itemsOrdered, { productTaxRateId: taxRate._id });
      const taxRateItemsPrices = _.map(taxRateItems, item => item.productSalePrice * item.quantity);
      const itemsPrice = _.sum(taxRateItemsPrices);
      const taxPayable = _.multiply(taxRate.taxRateAmount / 100, itemsPrice);
      return {
        rate: taxRate.taxRateAmount.toFixed(2),
        itemsPrice: itemsPrice.toFixed(2),
        taxPayable: taxPayable.toFixed(2),
      };
    });
    const serviceCharge = _.find(itemsOrdered, { isService: true });
    const totalLessService = serviceCharge
      ? total.currentTotal - serviceCharge.productSalePrice
      : 0.0;
    const showTabDetails =
      _.get(currentTab, 'location') === 'Collection' || _.get(currentTab, 'location') === 'Delivery';

    return new Promise((resolve) => {

        printer.init({
            type: 'epson',
            interface: ip,
            characterSet: 'UK',
            removeSpecialCharacters: false,
            replaceSpecialCharacters: true,
            extraSpecialCharacters: { '£': 0x23 }
        });

        if(PRINTERTYPES.KITCHEN === printerType) {
            printer.setTextDoubleHeight();
        }
        
        if (showCompanyDetails && companyDetails && printerType === PRINTERTYPES.CLIENT) {
            printer.alignCenter();

            // console.log("\tINFO COMPANY\n\t-------------------")
            // receiptOptions.showCompanyName && (companyDetails.companyName)? console.log(`\t${companyDetails.companyName}`):'';
            receiptOptions.showCompanyName && (companyDetails.companyName)? printer.println(companyDetails.companyName):'';

            // receiptOptions.showCompanyAddress && (companyDetails.companyAddress)? console.log(`\t${companyDetails.companyAddress}`):'';
            receiptOptions.showCompanyAddress && (companyDetails.companyAddress)? printer.println(companyDetails.companyAddress):'';

            // receiptOptions.showCompanyEmail && (companyDetails.companyEmail)? console.log(`\t${companyDetails.companyEmail}`):'';
            receiptOptions.showCompanyEmail && (companyDetails.companyEmail)? printer.println(companyDetails.companyEmail):'';

            // receiptOptions.showCompanyPhone && (companyDetails.companyPhone)? console.log(`\t${companyDetails.companyPhone}`): '';
            receiptOptions.showCompanyPhone && (companyDetails.companyPhone)? printer.println(companyDetails.companyPhone): '';
        }

        console.log(`\t${moment().format('MMMM DD YYYY, hh:mm')}`);
        printer.println(moment().format('MMMM DD YYYY, hh:mm'));

        if (showTabDetails && currentTab) {
            printer.println("Customer: " + currentTab.name);
            printer.println(currentTab.address.line1);
            printer.println(currentTab.address.postcode);
            currentTab.location && printer.println("Location: " + currentTab.location);
            currentTab.captureTime && printer.println("Ordered: " + moment(currentTab.captureTime).format('MMMM DD YYYY, hh:mm:ss'));
        }

        printer.drawLine();
        printer.alignLeft();


        //IMPRESION DE PRODUCTOS --> CLIENTE Y COCINA


        console.log()
        console.log()
        console.log("IMPRESION DE TICKETS")
        console.log()
        console.log()


        // PRINT ALL PRODUCTS
        printArrayCategory(starterArray, "Starter",offerData, printerType, categories, false);
        printArrayCategory(mainArray, "Main",offerData, printerType, categories, false);
        printArrayCategory(dessertArray, "Dessert",offerData, printerType, categories, false);
        printArrayCategory(othersArray, "Others",offerData, printerType, categories, false);
         
        

        if(printerType === PRINTERTYPES.CLIENT) {
            printer.drawLine();

            _.map(total.offerPrices, offerPrice => {

                printer.leftRight(offerPrice.offerName, `£-${parseFloat(offerPrice.offerDiscount).toFixed(2)}`);
    
            });
    
            if (serviceCharge) {
                printer.leftRight("Service Charge: ", `£${serviceCharge.productSalePrice.toFixed(2)}`)
                printer.leftRight("Service Charge: ", `£${serviceCharge.productSalePrice.toFixed(2)}`)
            }
    
            printer.leftRight("Total:", `£${total.currentTotal}`);
            console.log("\n\tTotal:", `£${total.currentTotal}`)
    
            if (paymentMethod !== '') {
                printer.leftRight("Method: ", _.capitalize(paymentMethod));
            }
    
            if (receiptCardData && receiptCardData.type) {
    
                if (receiptCardData.type === 'Optomany') {
                    printer.println(_.get(receiptCardData, 'provider', ''));
                    printer.leftRight("Card Number", _.get(receiptCardData, 'cardNumber', ''))
                    printer.leftRight("REF:", _.get(receiptCardData, 'ref', ''));
                    printer.leftRight("PID:", _.get(receiptCardData, 'pid', ''));
                    printer.leftRight("MID:", _.get(receiptCardData, 'mid', ''));
                    printer.leftRight("TID:", _.get(receiptCardData, 'terminalId', ''));
                    printer.leftRight("AID:", _.get(receiptCardData, 'applicationIdentifier', ''));
                    printer.println("Approved");
                    printer.leftRight("Auth code:", _.get(receiptCardData, 'authCode', ''));
                } else { // PaymentSense
                    printer.println(_.get(receiptCardData, 'merchant', ''));
                    printer.println(_.get(receiptCardData, 'tid', ''));
                    printer.println(_.get(receiptCardData, 'table', ''));
                    printer.println(_.get(receiptCardData, 'handset', ''));
                    printer.println(_.get(receiptCardData, 'provider', ''));
                    printer.println(_.get(receiptCardData, 'cardNumber', ''));
                    printer.println(_.get(receiptCardData, 'applicationId', ''));
                    printer.println("PIN VERIFIED");
                    printer.leftRight("Auth code:", _.get(receiptCardData, 'authCode', ''));
                }
    
            }
    
            if (change && change !== 0) {
                printer.leftRight("Change: ", `£${change.toFixed(2)}`);
            }
    
            if (vatItems.length > 0 && receiptOptions.showVatNumber) {
                printer.println("VAT:");
                console.log("\tVAT:")
                _.map(vatItems, (vatInfo, key) => {
                    if(receiptOptions.showBreakDownVatNumber){
                        console.log(`\t£${vatInfo.itemsPrice} @ ${vatInfo.rate}% = £${vatInfo.taxPayable} `)
                        printer.println(`£${vatInfo.itemsPrice} @ ${vatInfo.rate}% = £${vatInfo.taxPayable} `);
                    } else {
                        console.log(`\t£${vatInfo.taxPayable} `)
                        printer.println(`£${vatInfo.taxPayable} `);
                    }
                    
                });
            }

        } // END IF PRINTVIEW

        

        printer.drawLine();
        printer.alignCenter();

        if (receiptOptions.showCompanyVat && vatItems.length > 0 && printerType === PRINTERTYPES.CLIENT) {
            printer.println(`VAT Number: ${_.get(companyDetails, 'companyVatNumber', 0)} `)
        }

        if (receiptOptions.showReceiptNumber) {
            printer.println(`Num. ticket: ${_.get(companyDetails, 'receiptNumber', 0)}`);
        }

        if (currentTab) {
            printer.println(`${_.get(currentTab, 'name', 'None')}`)
        }

        if (receiptOptions.customText && printerType === PRINTERTYPES.CLIENT) {
            printer.println(receiptOptions.customText);
        }

        //system.tillImage =  system.tillImage.replace(/^data:image\/[a-z]+;base64,/, "");

        console.log("Imprimiendo ... ");
        //console.log(printer.getBuffer().toString('utf8'));
        printer.openCashDrawer();
        printer.cut();
        //console.log(printer.getText())
        
        printer.execute(() => {
            printer.clear();
            
            resolve(true);
        });
               
        /*printer.printImageBuffer(Buffer.from(system.tillImage, 'base64'), () => {
            printer.cut();
            printer.execute(() => {
                resolve(true);
            });            
        })*/

    });


};





///////FUNCTION TO PRINT ARRAY OF ITEMS

// 
function printArrayCategory(arrayCat, category, offerData,printerType,categories,cashPrinter = false){

    console.log(`array longitud: ${arrayCat.length}`)
    // console.log(arrayCat)
    if(arrayCat.length){

        if(category){
            console.log(`\n\t\t${category}`);
            console.log('\t-----------------');
            printer.println(category);
            printer.println("---------------------")
        }


        _.map(arrayCat, (item, key) => {
    
            if (item.isService) return '';
            
            printer.alignCenter();
    
    
            const basePrice = item.quantity * item.productSalePrice;
            const totalVariantPrice = _.chain(item.selectedVariants)
            .map(variant => (variant.default ? -Math.abs(variant.price) : variant.price))
            .sum()
            .multiply(item.quantity)
            .value();
                
    
                
            const price = parseFloat(basePrice + totalVariantPrice).toFixed(2);
            //let offer = LocalOffers

            var idx = offerData.findIndex((offerD) => item.itemId === offerD.itemId || item.productCategoryId === offerD.categoryId);
            let offer = false;
            if(idx >= 0) {
                offer = offerData[idx].offer;
            }
                
    
            let quantityExist = (item.productDiffQuantity === undefined)? 0: item.productDiffQuantity;

            //console.log('quantityExist: ' + quantityExist)

            let quantityAll = quantityExist >= 1 ? quantityExist : item.quantity;

            var table = [{
                text: item.productTitle,
                align: "LEFT",
                width: (printerType === PRINTERTYPES.CLIENT ? 0.65: 0.90)
            },{
                text: "x" + (printerType === PRINTERTYPES.KITCHEN ? quantityAll: item.quantity),
                align: "CENTER",
                width: 0.05
            }];


            if(printerType === PRINTERTYPES.CLIENT) {
                
                table.push({
                    text: ((!item.isDiscount) ? '£' + price : price + '%'),
                    align: "RIGHT",
                    width: 0.25
                });

            }
            
    
    
    
    
            //CHECK CATEGORIES

            let isCKitchen = categories.some((cat)=>{
                return cat.id === item.productCategoryId;
            });

            
            //Send to Kitchen
            //Only print in Kitchen a product [{categoryKitchen:true} or {item.isKitchenOrder: true}]

            if((printerType === PRINTERTYPES.KITCHEN && isCKitchen) || 
                (item.isKitchenOrder && printerType === PRINTERTYPES.KITCHEN)){

                //ONLY PRINT IF ITEM.ISPRINTED IS FALSE
                if(!item.isPrinted || item.productDiffQuantity > 0){
                    
                    console.log(`\t x${quantityAll} \t${item.productTitle} ` )
                    printer.bold(true);
                    printer.tableCustom(table);
                    printer.bold(false);
                    
                    printer.setTypeFontB();
                    
                    _.map(item.selectedVariants, (variant) => {
                        printer.println(' ' + (variant.default ? '-' : '+') + ' ' + variant.name + ((printerType === PRINTERTYPES.CLIENT) ? '\t £' + variant.price.toFixed(2) : ''));
                    });
                    
                    printer.setTypeFontA();  
                }

            }
            
            ///////////////////////
            if(printerType === PRINTERTYPES.CLIENT || cashPrinter){
                console.log(`\t x${quantityAll} \t${item.productTitle} ` )
                printer.bold(true);
                printer.tableCustom(table);
                printer.bold(false);

                printer.setTypeFontB();

                _.map(item.selectedVariants, (variant) => {
                    printer.println(' ' + (variant.default ? '-' : '+') + ' ' + variant.name + ((printerType === PRINTERTYPES.CLIENT) ? '\t £' + variant.price.toFixed(2) : ''));
                });
            
                if(offer && printerType === PRINTERTYPES.CLIENT) {
                    printer.println('Offer: ' + offer.offerTitle + ' ' + (offer.offerCount > 1 ? '(x' + offer.offerCount + ')' : ''));
                }
            
                printer.setTypeFontA();  
            }
            
            // NOTA: Variantes, cambiar tamaño letra
            //printer.beep();
    
            
    
        });
    }
}


function holaMundo(){
    console.log("Hola mundo");
}

function holaMundo1(){
    console.log("Hola mundo");
}
function holaMundo2(){
    console.log("Hola mundo");
}
function holaMundo3(){
    console.log("Hola mundo");
}
function holaMu44ndo(){
    console.log("Hola mundo");
}

 mensaje(()=>{console.log("mensaje")});


 mensaje1(()=>{console.log("mensaje")});
 mensaj2e(()=>{console.log("mensaje")});
 mensaje311(()=>{console.log("mensaje")});


 function h3333olaMu44ndo(){
    console.log("Hola mundo");
}

mens333333aje(()=>{console.log("mensaje")});

console.log("todo")
console.log("todo")
console.log("todo")
console.log("todo")
console.log("todo")

console.log("todo")
console.log("feature2")

console.log("feature2")
console.log("feature2")
console.log("feature2")
console.log("todo")
console.log("todo")


console.log("todo")

console.log("todo")
console.log("feature2")

console.log("feature")

